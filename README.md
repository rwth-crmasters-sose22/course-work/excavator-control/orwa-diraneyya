excavator-control-assignment
============================

### About

This is Orwa's submission to the excavator control assignment.

The name of the repo follows the "`first`-`last`" naming convention and in this case it is `orwa-diraneyya` because my `first` name is "orwa" and my `last` name is "diraneyya" (all in small caps, and no non-latin characters).

I love this project.